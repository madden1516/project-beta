import React, { useEffect, useState } from 'react';

function ModelList() {

    const [models, setModels] = useState([])

    const fetchModelsData = async () => {
        const url = `http://localhost:8100/api/models/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }


    useEffect(() => {
        fetchModelsData();
    }, []);

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Model Pic</th>
                    </tr>
                </thead>
                <tbody>
                    {models ? models.map(model => {
                        return (
                            <tr key={model.id}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td>
                                    <img src={model.picture_url} className="card-img-top" />
                                </td>
                            </tr>
                        );
                    }) : <tr><td>"Aww, nothng's there"</td></tr>}
                </tbody>
            </table>
        </>
    );
    }

export default ModelList
