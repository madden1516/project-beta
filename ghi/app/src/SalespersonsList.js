import { useState, useEffect } from 'react';

function SalespersonsList() {
    const [salespeople, setSalespeople] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople([...data.salespeople]);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee id</th>
                        <th>First name</th>
                        <th>Last name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople ? salespeople.map(salesperson => {
                        return(
                            <tr key={salesperson.employee_id}>
                                <td>{salesperson.employee_id}</td>
                                <td>{salesperson.first_name}</td>
                                <td>{salesperson.last_name}</td>
                            </tr>
                        );
                    }): <tr><td>There's nothing here</td></tr>}
                </tbody>
            </table>
        </>
    )
}

export default SalespersonsList;