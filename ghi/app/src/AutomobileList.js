
import React, { useEffect, useState } from 'react';

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);
    const fetchData = async () => {
        const url = `http://localhost:8100/api/automobiles/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles([...data.autos])
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles ? automobiles.map(auto => {
                        let sold = ''
                        if (auto.sold === true){
                            sold = 'Yes'
                        } else {
                            sold = 'No'
                        }
                        return(
                            <tr key={auto.vin}>
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.manufacturer.name}</td>
                                <td>{auto.model.name}</td>
                                <td>{sold}</td>
                            </tr>
                        )
                    }):<tr><td>There's nothing here</td></tr>}
                </tbody>
            </table>
        </>
    )

} export default AutomobileList
