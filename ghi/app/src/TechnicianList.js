import { useState, useEffect } from 'react';

function TechnicianList({technicians}) {

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians ? technicians.map(technician => {
                        return(
                            <tr key={technician.employee_id}>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                                <td>{technician.employee_id}</td>
                            </tr>
                        );
                    }): <tr><td>Nothing's here!!!!</td></tr>}
                </tbody>
            </table>
        </>
    )
}

export default TechnicianList;
