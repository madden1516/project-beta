import { useState, useEffect } from 'react';

function ManufacturersList() {
    const [manufacturers, setManufacturers] = useState([]);
    const fetchData = async () => {
        const url = `http://localhost:8100/api/manufacturers/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers([...data.manufacturers])
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <>
            <h1>Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers ? manufacturers.map(manufacturer => {
                        return(
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>

                            </tr>
                        )
                    }):<tr><td>There's nothing here</td></tr>}
                </tbody>
            </table>
        </>
    )
} export default ManufacturersList