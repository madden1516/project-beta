import { useState, useEffect } from 'react';
import { Outlet} from "react-router-dom";


function SalesHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState('')
    const [sales, setSales] = useState([]);

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }
    const fetchSalespersonData = async () => {
        const url = `http://localhost:8090/api/salespeople/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople([...data.salespeople]);
        }
    }
    const fetchSalesData = async () => {
        const url = `http://localhost:8090/api/sales/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSales([...data.sales]);
        }
    }
    useEffect(() => {
        fetchSalespersonData();
        fetchSalesData();
    }, []);
    return(
        <>
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Salesperson sales history</h1>
                            <form>
                                <div className="mb-3">
                                    <select onChange={handleSalespersonChange} name="salesperson" required id="salesperson" className="form-select">
                                    <option defaultValue="">Choose a salesperson</option>
                                    {salespeople.map(salesperson => {
                                        return (
                                            <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                                {salesperson.first_name} {salesperson.last_name}
                                            </option>
                                        );
                                    })}
                                    </select>
                                </div>
                            </form>
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Salesperson</th>
                                        <th>Customer</th>
                                        <th>VIN</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {sales ? sales.map(sale => {
                                        if (sale.salesperson.employee_id == salesperson) {
                                            return(

                                                <tr key={sale.id}>
                                                    <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                                    <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                                    <td>{sale.automobile.vin}</td>
                                                    <td>${sale.price}</td>
                                                </tr>
                                            );
                                        }
                                    }): <tr><td>There's nothing here</td></tr>}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
} export default SalesHistory
