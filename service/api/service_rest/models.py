from tkinter import CASCADE
from django.db import models
from django.urls import reverse

# Create your models here.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.AutoField(primary_key=True, unique=True)


class Appointment(models.Model):
    date_time = models.DateField(default=True)
    reason = models.TextField(max_length=500)
    status = models.CharField(max_length=10, blank=True)
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=200)

    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
        null=True
    )
    # automobile = models.ForeignKey(
    #     AutomobileVO,
    #     related_name="appointments",
    #     on_delete=models.CASCADE,
    #     null=True
    # )


    
