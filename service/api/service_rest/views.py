from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    TechnicianListEncoder,
    TechnicianDetailEncoder,
    AppointmentDetailEncoder,
)

from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()

        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_show_technicians(request, pk):
    if request.method == "GET":
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(employee_id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    
@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        automobiles = AutomobileVO.objects.all()
        for appointment in appointments:
            vip = "No"
            for automobile in automobiles:
                if appointment.vin == automobile.vin:
                    vip = 'Yes'
                    break
            appointment.vip = vip

        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = content["technician"]
            technicians = Technician.objects.get(employee_id=technician)
            content["technician"] = technicians
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )   
        except:
            response = JsonResponse(
                {"message": "Could not create the appointment"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET"])
def api_show_appointments(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["PUT"])
def api_appointments_cancel(request, pk):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=pk)
        setattr(appointment, "status", "cancel")
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False
        )
    
@require_http_methods(["PUT"])
def api_appointments_finish(request, pk):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=pk)
        setattr(appointment, "status", "finish")
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False
        )
